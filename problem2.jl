### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 9e51f190-cbf1-11ec-14da-dd865694df42
#Problem 2

# ╔═╡ cd2ada90-fe48-4be0-af5e-ceb3636cdb13
struct Sfunction
	name::Any
	responseTime::Int64
	latency::Int64
	throughput::Int64
	succcessRate::Float32
	input::Any
	output::Any
end

# ╔═╡ aec47fb0-e4a1-434e-b3a0-6bb981a9614a
struct Action
	name::Any
	minimumResponseTime::Int64
	maximumlatency::Int64
	minimumThroughput::Int64
	minimumSuccessRate::Float32
	functAss::Any
	domain::Array{Sfunction}
	input::Any
	output::Any
end

# ╔═╡ af4bc9c8-b121-4937-9bf6-30b2f5cacae5
begin
	functions=[Sfunction(1, 20, 4, 40, 0.9, 10, 10 ),Sfunction(2, 30, 5, 50, 0.7, 10, 10),Sfunction(3, 40, 8, 50, 0.8, 10, 10),Sfunction(4, 50, 3, 50, 0.9, 10, 10)]
end

# ╔═╡ 2624c3f8-7e43-41e7-a586-7f1eeb2eeb98
begin
	A1 =[Action(1, 10, 30, 5, 0.8, nothing, functions, 10, 10 ), Action(2, 40, 6,  30, 0.6, nothing, functions, 10, 10), Action(3, 50, 40, 40, 0.6, nothing, functions, 10, 10), Action(4, 50, 50, 40, 0.8, nothing, functions, 10, 10)]
	
	
end

# ╔═╡ e3ddde42-a606-4929-90fd-faf147cc6f45
connections = [
	[1, 2],
	[2, 3],
	[3, 4],
]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═9e51f190-cbf1-11ec-14da-dd865694df42
# ╠═aec47fb0-e4a1-434e-b3a0-6bb981a9614a
# ╠═cd2ada90-fe48-4be0-af5e-ceb3636cdb13
# ╠═af4bc9c8-b121-4937-9bf6-30b2f5cacae5
# ╠═2624c3f8-7e43-41e7-a586-7f1eeb2eeb98
# ╠═e3ddde42-a606-4929-90fd-faf147cc6f45
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
